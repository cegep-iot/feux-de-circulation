#include <Arduino.h>
#include "GestionnaireDeCycle.h"
#include "Bouton/Bouton.h"
#include "Feu/Feu.h"
#include "DonneesEEPROM.h"
#include "Action/Action.h"

GestionnaireDeCycle::GestionnaireDeCycle(Bouton* p_boutonPieton1, Bouton* p_boutonPieton2, Bouton* p_potentiometre, Feu* p_feuxVoiture, Feu* p_feuPieton, DonneesEEPROM* p_donneesEEPROM): m_boutonPieton1(p_boutonPieton1),
                                                                                                                                                        m_boutonPieton2(p_boutonPieton2),
                                                                                                                                                        m_potentiometre(p_potentiometre),
                                                                                                                                                        m_feuVoiture(p_feuxVoiture),
                                                                                                                                                        m_feuPieton(p_feuPieton),
                                                                                                                                                        m_donneesEEPROM(p_donneesEEPROM)
{
    this->m_dernierEtatCyclePieton = false;
    this->m_dernierEtatCycleVoiture = false;
}

void GestionnaireDeCycle::tickBoutons(){
    this->m_boutonPieton1->tick();
    this->m_boutonPieton2->tick();
    this->m_potentiometre->tick();
}

void GestionnaireDeCycle::tick()
{
    //Vérifie les boutons
    this->tickBoutons();

    //Vérifie si le feu pieton est demandé
    bool feuPietonDemande = this->m_feuPieton->getFeuPietonEstDemande();

    //Regarde l'état des cycles en cours
    bool feuVoitureCycleEnCours = this->m_feuVoiture->getCycleEnCours();
    bool feuPietonCycleEnCours = this->m_feuPieton->getCycleEnCours();

    //Vérifie si on a besoin de mettre à jour les données
    this->verifierDonneesAMettreAJour(feuPietonCycleEnCours, feuVoitureCycleEnCours);

    //Si le feu pieton est demandé et que le cycle voiture est terminé, alors on execute le cycle du feu pieton.
    if(feuPietonDemande == true && feuVoitureCycleEnCours == false)
    {
        this->m_feuVoiture->setFeuPietonEstDemande(true);
        this->m_feuPieton->executer();
    }
    //S'il n'y a pas de cycle de feu pieton en cours, alors on execute le cycle de feu voiture.
    else if(feuPietonCycleEnCours == false)
    {
        this->m_feuVoiture->executer();
    }
}

void GestionnaireDeCycle::verifierDonneesAMettreAJour(bool p_feuPietonCycleEnCours, bool p_feuVoitureCycleEnCours)
{
    //Variables pour incrémenter les données à ajouter
    int cyclesPietons = 0;
    int cyclesVoitures = 0;

    //Vérifie s'il y a un cycle pieton complet qui vient d'être effectué
    if(p_feuPietonCycleEnCours != this->m_dernierEtatCyclePieton)
    {
        if(p_feuPietonCycleEnCours == false && this->m_dernierEtatCyclePieton == true)
        {
            cyclesPietons++;
        }
        this->m_dernierEtatCyclePieton = p_feuPietonCycleEnCours;
    }

    //Vérifie s'il y a un cycle voiture complet qui vient d'être effectué
    if(p_feuVoitureCycleEnCours != this->m_dernierEtatCycleVoiture)
    {
        if(p_feuVoitureCycleEnCours == false && this->m_dernierEtatCycleVoiture == true)
        {
            cyclesVoitures++;
        }
        this->m_dernierEtatCycleVoiture = p_feuVoitureCycleEnCours;
    }

    //S'il y a des cycles, alors on met à jour les données.
    if(cyclesPietons > 0 || cyclesVoitures > 0)
    {
        this->m_donneesEEPROM->mettreAJourDonnees(cyclesPietons, cyclesVoitures);
    }
}

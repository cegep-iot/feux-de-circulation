#pragma once
#include "Arduino.h"
#include "Action/Action.h"
#include "Feu/FeuPietons.h"

class ActionBoutonPresse : public Action {
    private:
        bool m_demandeEnCours;
        Feu* m_feuPietons;

    public:
        ActionBoutonPresse(Feu* p_feuPietons);
        virtual void executer();
};
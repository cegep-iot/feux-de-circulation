#include <Arduino.h>
#include "Program.h"

Program* prog;

void setup() {
    Serial.begin(9600);
    prog = new Program();
}

void loop() {
    prog->loop();
}
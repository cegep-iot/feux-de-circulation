#pragma once
#include "Arduino.h"
#include "Action/Action.h"
#include "DonneesEEPROM.h"

class ActionPotentiometreTourneMax : public Action {
    private:
        DonneesEEPROM* m_donneesEEPROM;
        float calculerStatistiques();
        void afficherStatistiques(float p_statistiquesPourcentage);

    public:
        ActionPotentiometreTourneMax(DonneesEEPROM* p_donneesEEPROM);
        virtual void executer();
};
#include <Arduino.h>
#include "Program.h"
#include "DEL.h"
#include "Bouton/Bouton.h"
#include "Bouton/BoutonPietons.h"
#include "Bouton/Potentiometre.h"
#include "GestionnaireDeCycle.h"
#include "Feu/Feu.h"
#include "Feu/FeuAutos.h"
#include "Feu/FeuPietons.h"
#include "Action/Action.h"
#include "Action/ActionPotentiometreTourneMax.h"
#include "Action/ActionPotentiometreTourneMin.h"
#include "Action/ActionBoutonPresse.h"

Program::Program()
{
    DEL* delRougeVoiture = new DEL(4);
    DEL* delJauneVoiture = new DEL(3);
    DEL* delVerteVoiture = new DEL(2);
    DEL* delVertePieton = new DEL(6);
    DEL* delRougePieton = new DEL(7);

    Feu* feuVoiture = new FeuAutos(delRougeVoiture, delJauneVoiture, delVerteVoiture);
    Feu* feuPieton = new FeuPietons(delRougePieton, delVertePieton);

    DonneesEEPROM* persistance = new DonneesEEPROM();
    persistance->initialiser();

    Action* actionPieton = new ActionBoutonPresse(feuPieton);
    Action* actionPotMax = new ActionPotentiometreTourneMax(persistance);
    Action* actionPotMin = new ActionPotentiometreTourneMin(persistance);

    Bouton* bouton1 = new BoutonPietons(12,actionPieton);
    Bouton* bouton2 = new BoutonPietons(11,actionPieton);
    Bouton* potentiomentreStats = new Potentiometre(A0, actionPotMin, actionPotMax);

    GestionnaireDeCycle* gestionnaire = new GestionnaireDeCycle(bouton1, bouton2, potentiomentreStats, feuVoiture, feuPieton, persistance);
    this->m_gestionnaireDeCycle = gestionnaire;
}

//Toute la logique d'appel des cycles selon l'etat du gestionnaire de cycle et des requete du bouton pieton.
void Program::loop(){
    this->m_gestionnaireDeCycle->tick();
}

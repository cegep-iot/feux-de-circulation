#include <Arduino.h>
#include "EEPROM.h"
#include "DonneesEEPROM.h"

//Nous utiliserons deux index contenant les adresses où sont situées réellement les données afin d'optimiser les possibilités
//de stockage au maximum et de déplacer les données sur l'EEPROM lorsque leur adresse mémoire aura subit un certain nombre d'écritures,
//soit moins de 100000 pour s'assurer de la fiabilité des données et éviter au mieux leur corruption.

//L'adresse de l'index des nombres de cycle pietons est 0x0000.
//L'adresse de l'index des nombres de cycle autos est 0x0002.
//Les adresses des données vont de 0x0004 à 0x0201
//Les adresses de nombre d'écritures comptabilisées pour les emplacements mémoire des données vont de 0x0202 à 0x03FF

//Ainsi, chaque adresse de données aura son nombre d'écritures incrit à: adresse de données + 0x01FE (510)

//Pour inscrire des adresses mémoire dans les index nous utiliserons des entiers courts qui nécéssiteront 2 octets chacun.
//Pour les données et les nombres d'écritures nous auront besoin d'entiers longs pour une cohérence avec un usage à long terme,
//en prennant en compte les capacités physiques de l'EEPROM, soit 4 octets par emplacement défini.
//Nous serons donc capable avec une mémoire EEPROM neuve d'assurer 12 700 000 écritures de données.

DonneesEEPROM::DonneesEEPROM()
{
    ;
}

void DonneesEEPROM::initialiser()
{
    unsigned int adresseDonneesCyclePietons = 0;
    unsigned int adresseDonneesCycleAutos = 0;
    this->m_usee = false;

    //Ici, nous récupérons les adresses mémoire des données si elles existent.
    EEPROM.get(this->m_indexDonneesCyclePietons, adresseDonneesCyclePietons);
    EEPROM.get(this->m_indexDonneesCycleAutos, adresseDonneesCycleAutos);

    //Si elle n'existent pas, alors nous allons leur en attribuer une pour commencer à enregistrer les données.
    if(adresseDonneesCyclePietons == 0)
    {
        EEPROM.put(this->m_indexDonneesCyclePietons, 4);
    }

    if(adresseDonneesCycleAutos == 0)
    {
        EEPROM.put(this->m_indexDonneesCycleAutos, 8);
    }

    //Dans tous les cas nous allons vérifier le nombre d'écriture qu'a subit l'emplacement mémoire choisi, afin de s'assurer
    //de l'intégrité des données.
    this->verifierNbEcriture(this->m_indexDonneesCyclePietons);
    this->verifierNbEcriture(this->m_indexDonneesCycleAutos);

    //Lors de la vérification, si l'EEPROM à subit son nombre maximum d'écritures par emplacement mémoire alors
    //un message nous en informera.
    if(this->m_usee == true)
    {
        Serial.println("L'EEPROM est usée, veuillez changer de stockage, les données pourraient être corrompues.");
        Serial.println("L'écriture sur ce support est désormais désactivée...");
    }
}

void DonneesEEPROM::mettreAJourDonnees(long p_donneesCyclePietonsAAjouter, long p_donneesCycleAutosAAjouter)
{
    if(this->m_usee == false)
    {
        unsigned int adresseDonneesCyclePietons = 0;
        unsigned int adresseDonneesCycleAutos = 0;

        long donneesCyclePietons = 0;
        long donneesCycleAutos = 0;

        //On récupère les adresses des données, puis les données.
        EEPROM.get(this->m_indexDonneesCyclePietons, adresseDonneesCyclePietons);
        EEPROM.get(adresseDonneesCyclePietons, donneesCyclePietons);

        EEPROM.get(this->m_indexDonneesCycleAutos, adresseDonneesCycleAutos);
        EEPROM.get(adresseDonneesCycleAutos, donneesCycleAutos);

        //On cumule les comptes de cycles.
        donneesCyclePietons += p_donneesCyclePietonsAAjouter;
        donneesCycleAutos += p_donneesCycleAutosAAjouter;

        //On écrit les données.
        this->ecrireDonnees(donneesCyclePietons, donneesCycleAutos);
    }
}

void DonneesEEPROM::ecrireDonnees(long p_donneesCyclePietons, long p_donneesCycleAutos)
{
    if(this->m_usee == false)
    {
        //Avant d'écrire les données nous vérifions qu'il est toujours possible d'écrire à cet emplacement.
        this->verifierNbEcriture(this->m_indexDonneesCyclePietons);
        this->verifierNbEcriture(this->m_indexDonneesCycleAutos);

        unsigned int adresseDonneesCyclePietons = 0;
        unsigned long nbEcrituresCyclePietons = 0;
        unsigned int adresseDonneesCycleAutos = 0;
        unsigned long nbEcrituresCycleAutos = 0;

        //On récupère les données présentes ainsi que le nombre d'écritures de l'emplacement mémoire de chaque index
        //afin de les mettre à jour.
        EEPROM.get(this->m_indexDonneesCyclePietons, adresseDonneesCyclePietons);
        EEPROM.get(adresseDonneesCyclePietons + 510, nbEcrituresCyclePietons);

        EEPROM.get(this->m_indexDonneesCycleAutos, adresseDonneesCycleAutos);
        EEPROM.get(adresseDonneesCycleAutos + 510, nbEcrituresCycleAutos);
        
        //On écrit les données.
        EEPROM.update(adresseDonneesCyclePietons, p_donneesCyclePietons);
        EEPROM.update(adresseDonneesCycleAutos, p_donneesCycleAutos);
        
        //On incrémente et met à jour le nombre d'écritures pour les emplacements mémoire des données.
        nbEcrituresCyclePietons++;
        nbEcrituresCycleAutos++;

        EEPROM.update(adresseDonneesCyclePietons + 510, nbEcrituresCyclePietons);
        EEPROM.update(adresseDonneesCycleAutos + 510, nbEcrituresCycleAutos);
    }
}

long DonneesEEPROM::lireDonneesCyclePietons()
{
    unsigned int adresseDonneesCyclePietons = 0;
    long donneesCyclePietons = 0;

    EEPROM.get(this->m_indexDonneesCyclePietons, adresseDonneesCyclePietons);
    EEPROM.get(adresseDonneesCyclePietons, donneesCyclePietons);

    return donneesCyclePietons;
}

long DonneesEEPROM::lireDonneesCycleAutos()
{
    unsigned int adresseDonneesCycleAutos = 0;
    long donneesCycleAutos = 0;

    EEPROM.get(this->m_indexDonneesCycleAutos, adresseDonneesCycleAutos);
    EEPROM.get(adresseDonneesCycleAutos, donneesCycleAutos);

    return donneesCycleAutos;
}

void DonneesEEPROM::effacerDonnees()
{
    if(this->m_usee == false)
    {
        //Pour effacer nous allons écrire la valeur 0 à chaque octet des adresses mémoire des cycles.
        //Avant d'écrire les données nous vérifions qu'il est toujours possible d'écrire à cet emplacement.
        this->verifierNbEcriture(this->m_indexDonneesCyclePietons);
        this->verifierNbEcriture(this->m_indexDonneesCycleAutos);

        unsigned int adresseDonneesCyclePietons = 0;
        unsigned long nbEcrituresCyclePietons = 0;
        unsigned int adresseDonneesCycleAutos = 0;
        unsigned long nbEcrituresCycleAutos = 0;

        //On récupère les données présentes ainsi que le nombre d'écritures de l'emplacement mémoire de chaque index
        //afin de les mettre à jour.
        EEPROM.get(this->m_indexDonneesCyclePietons, adresseDonneesCyclePietons);
        EEPROM.get(adresseDonneesCyclePietons + 510, nbEcrituresCyclePietons);

        EEPROM.get(this->m_indexDonneesCycleAutos, adresseDonneesCycleAutos);
        EEPROM.get(adresseDonneesCycleAutos + 510, nbEcrituresCycleAutos);
        
        //On écrit la valeur 0 pour chaque octet des adresses.

        for(int octet = adresseDonneesCyclePietons; octet < adresseDonneesCyclePietons + 4; octet++)
        {
            EEPROM.update(octet, 0);
        }
        for(int octet = adresseDonneesCycleAutos; octet < adresseDonneesCycleAutos + 4; octet++)
        {
            EEPROM.update(octet, 0);
        }
        
        //On incrémente et met à jour le nombre d'écritures pour les emplacements mémoire des données.
        nbEcrituresCyclePietons++;
        nbEcrituresCycleAutos++;

        EEPROM.update(adresseDonneesCyclePietons + 510, nbEcrituresCyclePietons);
        EEPROM.update(adresseDonneesCycleAutos + 510, nbEcrituresCycleAutos);

        Serial.println("Les statistiques ont été réinitialisés.");
    }
}

void DonneesEEPROM::verifierNbEcriture(unsigned int p_indexDonnees)
{
    //On récupère le nombre d'écriture pour l'index donné.
    unsigned int adresseDonnees = 0;
    unsigned long nbEcritures = 0;

    EEPROM.get(p_indexDonnees, adresseDonnees);
    EEPROM.get(adresseDonnees + 510, nbEcritures);
    
    //Si le nombre maximum d'écritures est atteint, on selectionne une nouvelle adresse mémoire viable, s'il y a.
    if(nbEcritures >= this->m_nbEcrituresMax && this->m_usee == false)
    {
        this->selectionnerNouvelleAdresse(p_indexDonnees);
    }
}

void DonneesEEPROM::selectionnerNouvelleAdresse(unsigned int p_indexDonnees)
{
    unsigned int adresseDonnees = 0;   
    unsigned int adresseEnCours = 0;
    bool estAdresseDisponible = false;

    //On récupère l'adresse mémoire des données.
    EEPROM.get(p_indexDonnees, adresseDonnees);

    //Ici, nous itérons à travers les adresses d'emplacements de données afin de trouver possiblement un nouvel emplacement libre.
    //Nous itérons par 4, car nous avons besoin de 4 octets par adresse pour inscrire des entiers longs.
    for(adresseEnCours = adresseDonnees; adresseEnCours < 514 && !estAdresseDisponible; adresseEnCours = adresseEnCours + 4) {
        int donnees = 0;
        EEPROM.get(adresseEnCours, donnees);

        unsigned int nbEcritures = 0;
        EEPROM.get(adresseEnCours + 510, nbEcritures);

        //On averti à travers la console lorsque l'EEPROM arrive presque en fin de vie.
        if(adresseEnCours == EEPROM.length() - 520)
        {
            Serial.println("/!\\ Alerte: EEPROM usée à 98% /!\\");
            Serial.println("Vous devriez penser à changer de solution de stockage !!! Reste 200 000 écritures avant usure complète !!!");
        }
        //Si l'emplacement mémoire rempli les conditions pour accueillir des données sans en effacer d'autres, alors il est choisi.
        else if(donnees == 0 && nbEcritures < this->m_nbEcrituresMax)
        {
            estAdresseDisponible = true;
            adresseDonnees = adresseEnCours;
        }
        //Si, après avoir parcourru toutes les adresses, aucunes ne correspondent aux conditions requises alors nous déterminons que l'EEPROM
        //est usée et ne devrait plus être considérée comme fiable, nous désactivons la possibilité d'écrire dedans.
        else if(adresseEnCours == EEPROM.length() - 516 && nbEcritures >= this->m_nbEcrituresMax)
        {
            Serial.println("/!\\ Alerte: EEPROM usée à 100% /!\\");
            Serial.println("Vous devez changer de solution de stockage !!! Possibilité de corruption des données !!! Écriture désactivée !!!");
            this->m_usee = true;
            break;
        }
    }

    //Si l'EEPROM n'est pas usée, alors nous transférons les données.
    if(this->m_usee == false)
    {
        this->transfererDonnees(p_indexDonnees, adresseDonnees);
    }
}

void DonneesEEPROM::transfererDonnees(unsigned int p_indexDonnees, unsigned int p_nouvelleAdresse)
{
    unsigned int adresseDonnees;
    unsigned long nbEcritures;

    long donnees;

    //On récupère les données.
    EEPROM.get(p_indexDonnees, adresseDonnees);
    EEPROM.get(adresseDonnees, donnees);

    //On écrit les données dans leur nouvelle adresse.
    EEPROM.put(p_nouvelleAdresse, donnees);

    //On récupère le nombre d'écriture au cas où la nouvelle adresse aurait déjà subit des écritures.
    EEPROM.get(p_nouvelleAdresse + 510, nbEcritures);
    
    //On l'incrémente et la met à jour.
    nbEcritures++;
    EEPROM.put(p_nouvelleAdresse + 510, nbEcritures);

    //Enfin, on met à jour l'index avec la nouvelle adresse où se trouvent les données.
    EEPROM.put(p_indexDonnees, p_nouvelleAdresse);
}
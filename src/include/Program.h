#pragma once
#include <Arduino.h>
#include "GestionnaireDeCycle.h"

class Program{
    private:
        GestionnaireDeCycle* m_gestionnaireDeCycle;

    public:
        Program();
        void loop();
};
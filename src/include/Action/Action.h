#pragma once
#include "Arduino.h"

//Classe abstraite
class Action {
    public: 
        virtual void executer();
};
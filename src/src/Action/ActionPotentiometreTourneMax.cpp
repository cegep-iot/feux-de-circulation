#include "Arduino.h"
#include "Action/Action.h"
#include "Action/ActionPotentiometreTourneMax.h"
#include "DonneesEEPROM.h"

ActionPotentiometreTourneMax::ActionPotentiometreTourneMax(DonneesEEPROM* p_donneesEEPROM) : m_donneesEEPROM(p_donneesEEPROM)
{
    ;
}

float ActionPotentiometreTourneMax::calculerStatistiques()
{
    long cyclesPietons = this->m_donneesEEPROM->lireDonneesCyclePietons();
    long cyclesAutos = this->m_donneesEEPROM->lireDonneesCycleAutos();
    float statistiquesPourcentage = 0.00;

    if(cyclesPietons != 0 || cyclesAutos != 0)
    {
        statistiquesPourcentage = cyclesPietons * 100.0 / cyclesAutos;
    }
    
    return statistiquesPourcentage;
}

void ActionPotentiometreTourneMax::afficherStatistiques(float p_statistiquesPourcentage)
{
    Serial.println("Statistiques du nombre de cycle piétons par rapport au nombre de cycle autos: " + String(p_statistiquesPourcentage) + " %");
}

void ActionPotentiometreTourneMax::executer()
{
    float statistiquesPourcentage = this->calculerStatistiques();
    this->afficherStatistiques(statistiquesPourcentage);
}
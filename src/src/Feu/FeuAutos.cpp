#include <Arduino.h>
#include "Feu/FeuAutos.h"
#include "DEL.h"

FeuAutos::FeuAutos(DEL* p_delRouge, DEL* p_delJaune, DEL* p_delVerte) : m_delRouge(p_delRouge),
                                                                        m_delJaune(p_delJaune),
                                                                        m_delVerte(p_delVerte)
{
    this->setCycleEnCours(false);
    this->m_delRouge->allumer();
}

void FeuAutos::allumerJaune(){
    this->m_delJaune->allumer();
    this->eteindreRouge();
    this->eteindreVert();
}

void FeuAutos::allumerRouge(){
    this->m_delRouge->allumer();
    this->eteindreJaune();
    this->eteindreVert();
}

void FeuAutos::allumerVert(){
    this->m_delVerte->allumer();
    this->eteindreRouge();
    this->eteindreJaune();
}

void FeuAutos::eteindreJaune(){
    this->m_delJaune->eteindre();
}

void FeuAutos::eteindreRouge(){
    this->m_delRouge->eteindre();
}

void FeuAutos::eteindreVert(){
    this->m_delVerte->eteindre();
}

void FeuAutos::executer()
{
    if(this->getCycleEnCours() == true)
    {
        this->tick();
    }
    else
    {
        this->allumerRouge();
        long dateActuelle = millis();
        this->setDateDebutCycle(dateActuelle);
        this->setCycleEnCours(true);
    }
}

void FeuAutos::tick()
{
    long dateActuelle = millis();
    long dateDebutCycle = this->getDateDebutCycle();
    bool feuPietonEstDemande = this->getFeuPietonEstDemande();

    if(feuPietonEstDemande == false && dateActuelle - dateDebutCycle < 15000 )
    {
        this->allumerRouge();
    }
    else if (feuPietonEstDemande == true && dateActuelle - dateDebutCycle < 10000 )
    {
        this->allumerRouge();
    }
    else if(dateActuelle - dateDebutCycle > 15000 && dateActuelle - dateDebutCycle < 45000)
    {
        this->setFeuPietonEstDemande(false);
        this->allumerVert();
    }
    else if (dateActuelle - dateDebutCycle > 45000 && dateActuelle - dateDebutCycle < 50000)
    {
        this->allumerJaune();
    }
    else if (dateActuelle - dateDebutCycle > 50000)
    {
        this->allumerRouge();
        this->setCycleEnCours(false);
    }
}
#include <Arduino.h>
#include "Bouton/Bouton.h"
#include "Bouton/BoutonPietons.h"
#include "Action/Action.h"

BoutonPietons::BoutonPietons(uint8_t p_pin, Action* p_actionBoutonPresse) : m_pin(p_pin),
                                                                            m_actionBoutonPresse(p_actionBoutonPresse)
{
    pinMode(this->m_pin, INPUT);
    this->m_dernierEtatBouton = HIGH;
    this->m_dernierEtatStableBouton = HIGH;
    this->m_derniereDateChangement = 0;
}

//La fonction tick permet de gerer le rebond, determiner si le bouton est appuye ainsi qu'appeler l'action a exectuer lors de l'appuie d'un bouton. 
void BoutonPietons::tick(){
    int etatBouton = digitalRead(this->m_pin);
    long dateActuelle = millis();

    //Regarde si le bouton a changé d'état et met a jour les variables
    if (etatBouton != m_dernierEtatBouton) 
    {
        m_derniereDateChangement = dateActuelle;
        m_dernierEtatBouton = etatBouton;
    }

    //Permet de gerer s'il y a eu un rebond lors de l'appuie du bouton
    if(dateActuelle - this->m_derniereDateChangement > this->m_delaiMinPression)
    {
        //Regarde si le bouton est appuyé
        if(this->m_dernierEtatStableBouton == HIGH && etatBouton == LOW)
        {
            //Ici, ne fais rien.
        }
        //Regarde si le bouton est relaché
        else if(this->m_dernierEtatStableBouton == LOW && etatBouton == HIGH)
        {
            //L'action devrait se produire au relachement du bouton, cela reste un choix de conception
            this->m_actionBoutonPresse->executer();
        }
        
        //Met à jour le dernier etat stable du bouton pour la prochaine vérification
        m_dernierEtatStableBouton = etatBouton;
    }
}
#include <Arduino.h>
#include "Feu/FeuPietons.h"
#include "DEL.h"

FeuPietons::FeuPietons(DEL* p_delRouge, DEL* p_delVerte) : m_delRouge(p_delRouge),
                                                            m_delVerte(p_delVerte)
{
    this->setFeuPietonEstDemande(false);
    this->setCycleEnCours(false);
    this->m_delRouge->allumer();
}

void FeuPietons::allumerRouge(){
    this->m_delRouge->allumer();
    this->eteindreVert();
}

void FeuPietons::allumerVert(){
    this->m_delVerte->allumer();
    this->eteindreRouge();
}

void FeuPietons::eteindreRouge(){
    this->m_delRouge->eteindre();
}

void FeuPietons::eteindreVert(){
    this->m_delVerte->eteindre();
}

void FeuPietons::executer()
{
    if(this->getCycleEnCours() == true)
    {
        this->tick();
    }
    else
    {
        this->allumerRouge();
        long dateActuelle = millis();
        this->setDateDebutCycle(dateActuelle);
        this->setCycleEnCours(true);
    }
}

void FeuPietons::tick()
{
    long dateActuelle = millis();
    long dateDebutCycle = this->getDateDebutCycle();

    if (dateActuelle - dateDebutCycle < 20000)
    {
        this->allumerVert();
    }
    else if(dateActuelle - dateDebutCycle > 20000)
    {
        this->allumerRouge();
        this->setCycleEnCours(false);
        this->setFeuPietonEstDemande(false);
    }
}

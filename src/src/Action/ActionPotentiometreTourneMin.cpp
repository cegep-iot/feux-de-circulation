#include "Arduino.h"
#include "Action/Action.h"
#include "Action/ActionPotentiometreTourneMin.h"
#include "DonneesEEPROM.h"

ActionPotentiometreTourneMin::ActionPotentiometreTourneMin(DonneesEEPROM* p_donneesEEPROM) : m_donneesEEPROM(p_donneesEEPROM)
{
    ;
}

void ActionPotentiometreTourneMin::executer()
{
    this->m_donneesEEPROM->effacerDonnees();
}

#pragma once
#include "Feu/Feu.h"
#include "DEL.h"

class FeuAutos : public Feu {
    private:
        DEL* m_delRouge;
        DEL* m_delJaune;
        DEL* m_delVerte;

        void allumerRouge();
        void eteindreRouge();
        void allumerJaune();
        void eteindreJaune();
        void allumerVert();
        void eteindreVert();
        virtual void tick();

    public:
        FeuAutos(DEL* p_delRouge, DEL* p_delaJaune, DEL* p_delVerte);
        virtual void executer();
};


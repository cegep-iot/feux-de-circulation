#include "Arduino.h"
#include "Action/Action.h"
#include "Action/ActionBoutonPresse.h"
#include "Feu/FeuPietons.h"

ActionBoutonPresse::ActionBoutonPresse(Feu* p_feuPietons): m_feuPietons(p_feuPietons)
{
    this->m_demandeEnCours = false;
}

void ActionBoutonPresse::executer()
{
    this->m_feuPietons->setFeuPietonEstDemande(true);
}

#pragma once
#include "Arduino.h"
#include "Action/Action.h"
#include "DonneesEEPROM.h"

class ActionPotentiometreTourneMin : public Action {
    private:
        DonneesEEPROM* m_donneesEEPROM;
        void reinitialiserStatistiquesEEPROM();

    public:
        ActionPotentiometreTourneMin(DonneesEEPROM* p_donneesEEPROM);
        virtual void executer();
};
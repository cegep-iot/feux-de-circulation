#pragma once
#include <Arduino.h>
#include "Bouton/Bouton.h"
#include "Feu/FeuAutos.h"
#include "Feu/FeuPietons.h"
#include "Feu/Feu.h"
#include "DonneesEEPROM.h"
#include "Action/Action.h"

class GestionnaireDeCycle
{
    private: 
        long m_dateDebutCycle;
        bool m_dernierEtatCyclePieton;
        bool m_dernierEtatCycleVoiture;
        Bouton* m_boutonPieton1;
        Bouton* m_boutonPieton2;
        Bouton* m_potentiometre;
        Feu* m_feuVoiture;
        Feu* m_feuPieton;
        DonneesEEPROM* m_donneesEEPROM;
        void verifierDonneesAMettreAJour(bool p_feuPietonCycleEnCours, bool p_feuVoitureCycleEnCours);
        void tickBoutons();

    public:
        GestionnaireDeCycle(Bouton* p_boutonPieton1,Bouton* p_boutonPieton2, Bouton* p_potentiometre, Feu* p_feuVoiture, Feu* p_feuPieton, DonneesEEPROM* p_donneesEEPROM);
        void tick();
};

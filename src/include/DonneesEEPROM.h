#pragma once
#include "Arduino.h"
#include "EEPROM.h"

class DonneesEEPROM {
    private:
        const unsigned int m_indexDonneesCyclePietons = 0;
        const unsigned int m_indexDonneesCycleAutos = 2;
        const unsigned long m_nbEcrituresMax = 100000;
        bool m_usee;
        void verifierNbEcriture(unsigned int p_indexDonnees);
        void selectionnerNouvelleAdresse(unsigned int p_indexDonnees);
        void transfererDonnees(unsigned int p_indexDonnees, unsigned int p_nouvelleAdresse);
        void ecrireDonnees(long p_donneesCyclePietons, long p_donneesCycleAutos);

    public:
        DonneesEEPROM();
        void mettreAJourDonnees(long p_donneesCyclePietonsAAjouter, long p_donneesCycleAutosAAjouter);
        long lireDonneesCyclePietons();
        long lireDonneesCycleAutos();
        void effacerDonnees();
        void initialiser();
};
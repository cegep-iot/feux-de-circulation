#include <Arduino.h>
#include "Bouton/Bouton.h"
#include "Action/Action.h"
#include "Bouton/Potentiometre.h"

Potentiometre::Potentiometre(uint8_t p_pin, Action* p_actionPotentiometreTourneMin, Action* p_actionPotentiometreTourneMax) : m_pin(p_pin),
                                                                                                        m_actionPotentiometreTourneMin(p_actionPotentiometreTourneMin),
                                                                                                        m_actionPotentiometreTourneMax(p_actionPotentiometreTourneMax)
{
    this->m_dernierEtatPotentiometre = analogRead(this->m_pin);
    this->m_dernierEtatStablePotentiometre = analogRead(this->m_pin);

    Serial.println();
    this->m_actionPotentiometreTourneMax->executer();
}

void Potentiometre::tick()
{
    int etatPotentiometre = analogRead(this->m_pin);
    long dateActuelle = millis();

    //Regarde si le potentiometre a changé d'état et met a jour les variables
    if (etatPotentiometre != this->m_dernierEtatPotentiometre) 
    {
        this->m_derniereDateChangement = dateActuelle;
        this->m_dernierEtatPotentiometre = etatPotentiometre;
    }

    //Permet d'avoir un delai pour éviter les répétitons (même principe que le rebond d'un bouton)
    if(dateActuelle - this->m_derniereDateChangement > this->m_delaiMin)
    {
        //Regarde si le potentiometre est tourné au minimum soit, complètement vers la gauche (réinitialise les statistiques)
        if(this->m_dernierEtatStablePotentiometre > 6 && etatPotentiometre < 6)
        {
            this->m_actionPotentiometreTourneMin->executer();
        }
        //Regarde si le potentiometre est tourné au maximum soit, complètement vers la droite (affiche les statistiques)
        else if(this->m_dernierEtatStablePotentiometre < 1023 && etatPotentiometre == 1023)
        {
            this->m_actionPotentiometreTourneMax->executer();
        }
    //Met à jour le dernier etat stable du bouton pour la prochaine vérification
    this->m_dernierEtatStablePotentiometre = etatPotentiometre;
    }
}

# Travail Pratique: Feux de circulation

## Description du projet

La municipalité de « LongueVie » désire mettre en place un système de signalisation par feux de circulation. De nombreuses collisions entre véhicules et accidents impliquant des piétons blessés gravement sont survenus depuis l’élargissement de la chaussée à quatre voies.

Le « directeur de la sécurité routière de la municipalité »  vous a donc octroyé un mandat pour mettre en place un système de gestion des feux de circulation en remplacement des panneaux d’arrêt.

Votre tâche générale consiste à mettre en place, un programme de gestion du contrôle de la circulation.

Ce système comprend :

- Un « feu de circulation automobile » (ci-après nommé FeuxAutos) composé de 3 DELs (une rouge, une jaune et une verte) pour les véhicules automobiles
- Un « feu de piétons » (ci-après nommé FeuxPiétons) composé de 2 DELs (une rouge et une verte). Le FeuPiétons est commandé par des boutons placés de chaque côté de la chaussé
- Un microcontrôleur Arduino UNO et une carte de circuit électronique contenant les composants pour le bon fonctionnement du système
- Un document technique

### Mise en place des feux de circulation

FeuxAutos utilise le cycle rouge (1), vert (2) et jaune (3) :

![](img/cycle_sans_appel.png)

Quand un piéton appuie sur le bouton d’appel, sa demande est mémorisée dans le système. Quand le feu passe au rouge, on valide si une demande est en cours, si c’est le cas, on passe FeuxPiétons au vert pendant une certaine durée déterminée (1A). Le feu piéton passe ensuite au rouge pendant une autre durée déterminée (1B) et le cycle des feux automobile recommence (2, 3) :

![](img/cycle_avec_appel.png)

On voit ici que l’état 1 du cycle sans piéton a été transformé en deux états 1A et 1B.

Si le feu automobile est rouge durant la pression du bouton de piéton, le piéton doit attendre la prochaine validation au début du prochain cycle.

Pour les besoins de la simulation, les temps choisis sont :

| État | Durée (sec.) | FeuxAutos | FeuxPiétons |
| --- | --- | --- | --- |
| 1 | 15 | Rouge | Rouge |
| 1A | 20 | Rouge | Vert |
| 1B | 10 | Rouge | Rouge |
| 2 | 30 | Vert | Rouge |
| 3 | 5 | Jaune | Rouge |

### Analyse statistique

La municipalité veut connaître l'achalandage des piétons dans les rues et propose comme première approche de pouvoir afficher le pourcentage de nombre de cycle piéton par rapport au nombre de cycle automobile.

Ce pourcentage ne doit pas être réinitialisé à chaque redémarrage du système de contrôle de l'artère de circulation.

Vous devez trouver un moyen d'afficher facilement le pourcentage au démarrage du système et sur demande. Vous devez également trouver un moyen de réinitialiser le pourcentage à 0.

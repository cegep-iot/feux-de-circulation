#pragma once
#include "Arduino.h"
#include "Bouton/Bouton.h"
#include "Action/Action.h"

class Potentiometre : public Bouton {
    private:
        uint8_t m_pin;
        Action* m_actionPotentiometreTourneMin;
        Action* m_actionPotentiometreTourneMax;
        int m_dernierEtatPotentiometre;
        long m_derniereDateChangement;
        int m_dernierEtatStablePotentiometre;
        const int m_delaiMin = 25;

    public:
        Potentiometre(uint8_t p_pin, Action* p_actionPotentiometreTourneMin, Action* p_actionPotentiometreTourneMax);
        virtual void tick();
};
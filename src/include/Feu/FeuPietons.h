#pragma once
#include "Feu/Feu.h"
#include "DEL.h"

class FeuPietons : public Feu {
    private: 
        DEL* m_delRouge;
        DEL* m_delVerte;

        void allumerRouge();
        void eteindreRouge();
        void allumerVert();
        void eteindreVert();
        virtual void tick();

    public:
        FeuPietons(DEL* p_delRouge, DEL* p_delVerte);
        virtual void executer();
};


#include <Arduino.h>
#include "Feu/Feu.h"

Feu::Feu()
{
    ;
}

bool Feu::getCycleEnCours()
{
    return this->m_cycleEnCours;
}

void Feu::setCycleEnCours(bool p_valeur)
{
    this->m_cycleEnCours = p_valeur;
}

void Feu::setDateDebutCycle(long p_date)
{
    this->m_dateDebutCycle = p_date;
}

long Feu::getDateDebutCycle()
{
    return this->m_dateDebutCycle;
}

bool Feu::getFeuPietonEstDemande()
{
    return this->m_FeuPietonEstDemande;
}

void Feu::setFeuPietonEstDemande(bool p_valeur)
{
    this->m_FeuPietonEstDemande = p_valeur;
}
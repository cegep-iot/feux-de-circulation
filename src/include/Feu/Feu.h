#pragma once
#include "Arduino.h"
#include "DEL.h"

class Feu{
    private:
        bool m_cycleEnCours;
        long m_dateDebutCycle;
        bool m_FeuPietonEstDemande;

        virtual void tick();

    public:
        Feu();
        virtual void executer();
        virtual bool getCycleEnCours();
        virtual void setCycleEnCours(bool p_valeur);
        virtual long getDateDebutCycle();
        virtual void setDateDebutCycle(long p_date);
        virtual bool getFeuPietonEstDemande();
        virtual void setFeuPietonEstDemande(bool p_valeur);
};

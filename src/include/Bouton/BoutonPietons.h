#pragma once
#include "Arduino.h"
#include "Bouton/Bouton.h"
#include "Action/Action.h"

class BoutonPietons : public Bouton {
    private:
        uint8_t m_pin;
        Action* m_actionBoutonPresse;
        int m_dernierEtatBouton;
        long m_derniereDateChangement;
        int m_dernierEtatStableBouton;
        const int m_delaiMinPression = 25;

    public:
        BoutonPietons(uint8_t p_pin, Action* p_actionBoutonPresse);
        virtual void tick();
};
